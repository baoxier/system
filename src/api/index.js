import axios from 'axios'
axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/';

//添加请求拦截器
axios.interceptors.request.use(function(config){
    //在发送请求之前做某事
    let token=localStorage.getItem('mytoken')
    if(token){
         config.headers['Authorization']=token
       }
       return config;
    },function(error){
       //请求错误时做些事
       return Promise.reject(error);
    }
);



export const checkUser=params=>{
    return axios.post('login', params).then(res => res.data)
}
export const getUser=params=>{
    return axios.get('users',params).then(res=>res.data)
}
export const addUser=params=>{
    return axios.post('users',params).then(res=>res.data)
}
export const changeUserState=params=>{
    return axios.put(`users/${params.uid}/state/${params.type}`).then(res=>res.data)
}
export const editUser=params=>{
    return axios.put(`users/${params.id}`,params).then(res=>res.data)
}
export const delUserSubmit=params=>{
    return axios.delete(`users/${params.id}`).then(res=>res.data)
}
export const getRolelist=params=>{
    return axios.get('roles').then(res=>res.data)
}
export const assignDataSubmit=params=>{
    return axios.put(`users/${params.id}/role`,params).then(res=>res.data)
}

export const getAuthlist=params=>{
    return axios.get(`rights/${params.type}`,params).then(res=>res.data)
}
export const AuthRoles=(roleId,rids)=>{
    return axios.post(`roles/${roleId}/rights`,rids).then(res=>res.data)
}
//商品管理
export const getgoodslist=params=>{
    return axios.get('goods',params).then(res=>res.data)
}
export const getproductslist=params=>{
    return axios.get('categories',params).then(res=>res.data)
}