import Vue from 'vue'
import Router from 'vue-router'
//import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Welcome from '@/components/Welcome'
import Welcome2 from '@/components/Welcome2'
import Userlist from '@/components/Userlist'
import Rolelist from '@/components/Rolelist'
import Authlist from '@/components/Authlist'
import Products from '@/components/Products'
import Goodslist from '@/components/Goodslist'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/welcome',
      name: 'welcome',
      component: Welcome,
      children:[
        {path:'/welcome',name:'welcome2',component:Welcome2},
        {path:'/userlist',name:'userlist',component:Userlist},
        {path:'/rolelist',name:'rolelist',component:Rolelist},
        {path:'/authlist',name:'authlist',component:Authlist},
        {path:'/products',name:'products',component:Products},
        {path:'/goodslist',name:'goodslist',component:Goodslist}
      ]
    },
    
  ]
})
